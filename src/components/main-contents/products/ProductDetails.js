import React, { Component } from "react";
import Sidebar from "../../sidebar/Sidebar";
import Content from "../Content";
import PageHeading from "../PageHeading";
import DataTable from "./DataTable";
import Search from "./Search";
import Pagination from "./Pagination";

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      keyword: "",
      limit: 5
    };
  }

  onSearchData = datasearch => {
    console.log("search", datasearch);
    this.setState({
      keyword: datasearch
    });
  };
  choose(e) {
    this.setState(
      {
        limit: e.target.value
      },
      () => {
        console.log(this.state.limit);
      }
    );
  }
  render() {
    // console.log(this.state.totalItems);
    return (
      <div id="wrapper">
        <Sidebar />
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
            <Content />
            <div className="container-fluid">
              <PageHeading />
              <div className="card shadow mb-4">
                <div className="card-header py-3">
                  <h6 className="m-0 font-weight-bold text-primary">
                    DataTables Example
                  </h6>
                </div>
                <div className="card-body">
                  <div className="table-responsive">
                    <div
                      id="dataTable_wrapper"
                      class="dataTables_wrapper dt-bootstrap4"
                    >
                      <div class="row">
                        <div class="col-sm-12 col-md-6">
                          <div class="dataTables_length" id="dataTable_length">
                            <label>
                              Show{" "}
                              <select
                                onChange={this.choose.bind(this)}
                                name="dataTable_length"
                                aria-controls="dataTable"
                                class="custom-select custom-select-sm form-control form-control-sm"
                              >
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="50">50</option>
                              </select>{" "}
                              entries
                            </label>
                          </div>
                        </div>
                        <Search onSearchData={this.onSearchData} />
                      </div>

                      <DataTable
                        onChange={this.state.limit}
                        onSearchData={this.state.keyword}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductDetails;
