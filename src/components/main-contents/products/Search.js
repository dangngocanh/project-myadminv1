import React, { Component } from "react";

class Search extends Component {
  onSearchData = event => {
    var { value } = event.target;
    this.props.onSearchData(value);
  };
  render() {
    return (
      <div className="col-sm-12 col-md-6">
        <div id="dataTable_filter" className="dataTables_filter">
          <label>
            Search:
            {/* {this.state.keyword} */}
            <input
              type="search"
              onChange={this.onSearchData}
              class="form-control form-control-sm"
              placeholder=""
              aria-controls="dataTable"
            />
          </label>
        </div>
      </div>
    );
  }
}

export default Search;
