import React, { Component } from "react";
import Sidebar from "../../sidebar/Sidebar";
import Content from "../Content";
import axios from "axios";
import { Link } from "react-router-dom";
class UpdateProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: {},
      name: "",
      position: "",
      office: "",
      age: "",
      startdate: "",
      salary: "",
      show: false
    };
  }

  componentDidMount() {
    //  let match = useRouteMatch("/product-details/:id");
    var idProducts = this.props.match.params.id;
    ///console.log("Id là :", idProducts);
    axios
      .get(`http://5d959642a824b400141d1bc6.mockapi.io/Products/${idProducts}`)
      .then(res => {
        const persons = res.data;
        this.setState(
          {
            persons,
            name: persons.name,
            position: persons.Prosion,
            office: persons.Office,
            age: persons.Age,
            startdate: persons.StartDate,
            salary: persons.Salary
          },
          () => {
            console.log(this.state);
          }
        );
      })
      .catch(error => console.log(error));
  }
  // ---------------------------------
  onChangeValue = event => {
    var value = event.target.value;
    var name = event.target.name;
    //console.log(value, name);
    this.setState({
      [name]: value
    });
    console.log(this.state);
  };
  // ---------------------------------
  onSubmit(e) {
    this.setState({
      show: true
    });
    // alert("Update thành công");
    try {
      //const { persons } = this.state;
      var idProducts = this.props.match.params.id;
      e.preventDefault();
      const productdetails = {
        name: this.state.name,
        Age: this.state.age,
        Salary: this.state.salary,
        Prosion: this.state.position,
        Office: this.state.office,
        StartDate: this.state.startdate
      };
      console.log(this.state);
      axios
        .put(
          `http://5d959642a824b400141d1bc6.mockapi.io/Products/${idProducts}`,
          productdetails
        )
        .then(res => {
          this.setState({
            show: false
          });
          console.log(res.data);
        });
    } catch (error) {
      console.log("Lỗi");
    }
  }

  ///-------------------------------

  render() {
    // console.log(this.props);
    const { name, persons, show } = this.state;
    return (
      <div id="wrapper">
        <Sidebar />
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
            <Content />
            <div className="container-fluid">
              <div class="row justify-content-md-center">
                <div class="col col-md-10">
                  <div className="card shadow mb-4">
                    <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-dark">
                        Thông tin nhân viên :{" "}
                        {name ? name.toUpperCase() : "Loadding"}
                      </h6>
                    </div>
                    <div className="card-body">
                      <form>
                        <div className="form-group">
                          <label for="formGroupExampleInput">Name:</label>
                          <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput"
                            name="name"
                            defaultValue={persons.name}
                            onChange={this.onChangeValue}
                          />
                        </div>
                        <div className="form-group">
                          <label for="formGroupExampleInput2">Position:</label>
                          <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput2"
                            name="position"
                            defaultValue={persons.Prosion}
                            onChange={this.onChangeValue}
                          />
                        </div>
                        <div className="form-group">
                          <label for="formGroupExampleInput2">Office:</label>
                          <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput2"
                            name="office"
                            defaultValue={persons.Office}
                            onChange={this.onChangeValue}
                          />
                        </div>
                        <div className="form-group">
                          <label for="formGroupExampleInput2">Age:</label>
                          <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput2"
                            name="age"
                            defaultValue={persons.Age}
                            onChange={this.onChangeValue}
                          />
                        </div>
                        <div className="form-group">
                          <label for="formGroupExampleInput2">
                            Start Date:
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput2"
                            name="startdate"
                            defaultValue={persons.StartDate}
                            onChange={this.onChangeValue}
                          />
                        </div>
                        <div className="form-group">
                          <label for="formGroupExampleInput2">Salary:</label>
                          <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput2"
                            name="salary"
                            defaultValue={persons.Salary}
                            onChange={this.onChangeValue}
                          />
                        </div>
                        <button
                          type="submit"
                          onClick={event => this.onSubmit(event)}
                          class="btn btn-success"
                        >
                          {show === true ? (
                            <i class="fa fa-refresh fa-spin"></i>
                          ) : (
                            ""
                          )}
                          Update
                        </button>

                        <Link to="/products" className="ml-3 btn btn-primary">
                          Back
                        </Link>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdateProduct;
