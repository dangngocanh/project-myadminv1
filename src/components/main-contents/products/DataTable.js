import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";

class DataTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      keyworddata: "",
      searchpersons: [],
      activePage: 1,
      totalItemsCount: 0,
      limit: 5,
      totalPage: 0
    };
  }
  componentDidMount() {
    axios
      .get(`http://5d959642a824b400141d1bc6.mockapi.io/Products`)
      .then(res => {
        const persons = res.data;
        const arr = persons.slice(0, 5);
        this.setState(
          {
            persons,
            searchpersons: arr,
            totalItemsCount: persons.length
          },
          () => {
            console.log(this.state.totalItemsCount);
          }
        );
      })
      .catch(error => console.log(error));
  }
  componentWillReceiveProps(nextProps) {
    this.setState(
      {
        keyworddata: nextProps.onSearchData,
        limit: nextProps.onChange
      },
      () => {
        console.log(this.state.limit);
        var begin = 0;
        var end =
          this.state.limit !== 50 ? this.state.limit : this.state.data.length;
        const _arr = this.state.persons.slice(begin, end);
        this.setState({
          searchpersons: _arr,
          activePage: 1
        });
        let totalPage = this.state.persons.length % this.state.limit;
        console.log(totalPage);
        const _totalPage = this.state.persons.length / this.state.limit;
        console.log(_totalPage);
        totalPage =
          totalPage === 0
            ? totalPage
            : (_totalPage + 1).toString().split(".")[0];
        this.setState({
          totalPage: totalPage
        });
      }
    );
    this.showData(nextProps.onSearchData);
    console.log(nextProps.onSearchData);
  }
  showData = data => {
    console.log(data);
    var { persons } = this.state;
    var arr = [];
    persons.forEach(element => {
      if (element.name.toLowerCase().indexOf(data.toLowerCase()) > -1) {
        arr.push(element);
        console.log(arr);
      }
    });
    const arr2 = arr.slice(0, 5);
    this.setState(
      {
        searchpersons: arr2
      },
      () => {
        this.setState({
          searchpersons: arr2
        });
      }
    );
  };
  popUp = item => {
    console.log("hiện", item);
  };

  slugURL = str => {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, "a");
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, "e");
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, "i");
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, "o");
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, "u");
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, "y");
    str = str.replace(/(đ)/g, "d");

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, "");

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, "-");

    // xóa phần dự - ở đầu
    str = str.replace(/^-+/g, "");

    // xóa phần dư - ở cuối
    str = str.replace(/-+$/g, "");

    // return
    return str;
  };

  onDelete(item, index) {
    var { searchpersons } = this.state;
    searchpersons.splice(index, 1);

    if (window.confirm("Bạn có muốn xóa không????") === true) {
      axios
        .delete(
          `http://5d959642a824b400141d1bc6.mockapi.io/Products/${item.id}`
        )
        .then(res => {
          this.setState({
            searchpersons
          });
        });
    } else {
      return false;
    }
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    var page = pageNumber;
    var perPage = 5; // hiển thị bn item
    var begin = (page - 1) * perPage;
    var end = page * perPage;
    const arr = this.state.persons.slice(begin, end);
    this.setState({
      searchpersons: arr,
      activePage: pageNumber
    });
  }

  render() {
    // this.props.totalItems(this.state.searchpersons.length);
    return (
      <div class="table-responsive">
        <table
          class="table table-bordered"
          id="dataTable"
          width="100%"
          cellspacing="0"
        >
          <thead>
            <tr>
              <th>Name</th>
              <th>Position</th>
              <th>Office</th>
              <th>Age</th>
              <th>Start date</th>
              <th>Salary</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Name</th>
              <th>Position</th>
              <th>Office</th>
              <th>Age</th>
              <th>Start date</th>
              <th>Salary</th>
            </tr>
          </tfoot>
          <tbody>
            {/* {listdata} */}
            {/* {demo} */}
            {this.state.searchpersons.map((e, i) => {
              return (
                <tr key={i} onClick={() => this.popUp(i)}>
                  <td>
                    <Link
                      to={
                        "/product-details/" + this.slugURL(e.name) + "." + e.id
                      }
                    >
                      {e.name}
                    </Link>
                  </td>
                  <td>{e.Prosion}</td>
                  <td>{e.Office}</td>
                  <td>{e.Age}</td>
                  <td>{e.StartDate}</td>
                  <td>
                    ${e.Salary}
                    <button
                      className="btn btn-danger float-right"
                      onClick={() => this.onDelete(e, i)}
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="row">
          <div className="col-sm-12 col-md-5">
            <div
              class="dataTables_info"
              id="dataTable_info"
              role="status"
              aria-live="polite"
            >
              Showing 1 to {this.state.limit} of {this.state.totalItemsCount}{" "}
              entries
            </div>
          </div>
          <div className="col-sm-12 col-md-7">
            <div
              class="dataTables_paginate paging_simple_numbers"
              id="dataTable_paginate"
            >
              <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.limit} // hiện page 5 items
                totalItemsCount={this.state.totalItemsCount}
                pageRangeDisplayed={this.state.totalPage} // hiển thị page 4
                innerClass={"pagination justify-content-end"}
                linkClass={"page-link"}
                prevPageText={"prev"}
                nextPageText={"next"}
                activeClass={"active"}
                itemClass={"paginate_button page-item "}
                onChange={pageNumber => this.handlePageChange(pageNumber)}
              />
              {/* <Pagination /> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DataTable;
