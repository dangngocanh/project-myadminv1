import React, { Component } from "react";
import Sidebar from "./sidebar/Sidebar";
import Content from "./main-contents/Content";
import PageHeading from "./main-contents/PageHeading";
import Statistical from "./main-contents/Statistical";
import ChartStatistical from "./main-contents/ChartStatistical";

class Home extends Component {
  render() {
    return (
      <div id="wrapper">
        <Sidebar />
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
            <Content />
            <div className="container-fluid">
              <PageHeading />
              <Statistical />
              <ChartStatistical />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
