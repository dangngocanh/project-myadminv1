import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
// import Sidebar from "./components/sidebar/Sidebar";
// import Content from "./components/main-contents/Content";
// import PageHeading from "./components/main-contents/PageHeading";
// import Statistical from "./components/main-contents/Statistical";
// import ChartStatistical from "./components/main-contents/ChartStatistical";
import RouterURL from "./routerurl/RouterURL";
import Home from "./components/Home";
//import ProductDetails from "./components/main-contents/products/ProductDetails";

class App extends Component {
  render() {
    return (
      <Router>
        <RouterURL>
          <Home />
        </RouterURL>
      </Router>
    );
  }
}

export default App;
