import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "../App.css";
import ProductDetails from "../components/main-contents/products/ProductDetails";
import Home from "../components/Home";
import UpdateProduct from "../components/main-contents/products/UpdateProduct";
class RouterURL extends Component {
  NoMatchPage = () => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="error-template">
              <h1>Oops!</h1>
              <h2>404 Not Found</h2>
              <div className="error-details">
                Sorry, an error has occured, Requested page not found!
              </div>
              <div className="error-actions">
                <a href="/" className="btn btn-primary btn-lg">
                  <span className="glyphicon glyphicon-home" />
                  Take Me Home{" "}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div>
        <Switch>
          {/* exact là đường dẫn chính xác */}
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/products">
            <ProductDetails />
          </Route>
          <Route
            exact
            path="/product-details/:slug.:id"
            component={UpdateProduct}
          ></Route>
          <Route component={this.NoMatchPage} />
        </Switch>
      </div>
    );
  }
}

export default RouterURL;
